﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SDD.Events;

#region GameManager Events

public class GameMenuEvent : SDD.Events.Event { }
public class GamePlayEvent : SDD.Events.Event { }
public class GamePauseEvent : SDD.Events.Event { }
public class GameResumeEvent : SDD.Events.Event { }
public class GameOverEvent : SDD.Events.Event { }
public class GameVictoryEvent : SDD.Events.Event { }

#endregion
