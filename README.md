Système d'Event C# pour Unity 3D :

S'inscrire à un Event :
EventManager.Instance.AddListener<SomethingHappenedEvent>(OnSomethingHappened);

Se désinscrire d'un Event :
EventManager.Instance.RemoveListener<SomethingHappenedEvent>(OnSomethingHappened);

Déclancher un Event :
EventManager.Instance.Raise(new SomethingHappenedEvent());